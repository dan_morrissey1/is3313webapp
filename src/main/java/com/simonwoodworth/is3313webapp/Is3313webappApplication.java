package com.simonwoodworth.is3313webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Is3313webappApplication {

    public static void main(String[] args) {
        SpringApplication.run(Is3313webappApplication.class, args);
    }

}
